package com.resoft.chatgpt;

import okhttp3.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.fusesource.jansi.AnsiConsole;

public class Chatgpt4Excel {
	// ANSI转义码前缀
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";

	private static final OkHttpClient httpClient = new OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS) // 增加连接超时时间
			.readTimeout(100, TimeUnit.SECONDS) // 增加读取超时时间
			.build();

	private static final String OPENAI_API_KEY = System.getenv("OPENAI_API_KEY");
	private static final String OPENAI_BASE_URL = System.getenv("OPENAI_BASE_URL");
	private static final String OPENAI_BASE_URL_DEFAULT = "https://api.openai.com";
	private static final String OPENAI_BASE_URL_POST = "v1/chat/completions";

	private static final String STATUS_TODO = "待回答";
	private static final String STATUS_FINISHED = "已完成";
	private static final String STATUS_ERROR = "发生错误";

	private static final int WRITE_INTERVAL = 50;

	public static void main(String[] args) throws IOException {
		AnsiConsole.systemInstall(); // 安装Jansi的ANSI控制台支持

		// 如果没有提供命令行参数，显示帮助信息
		if (args.length == 0) {
			printUsage();
			return;
		}

		String excelFilePath = null;
		String modelName = "gpt-3.5-turbo";
		String baseUrl = null;
		boolean verbose = false;

		// 解析命令行参数
		for (int i = 0; i < args.length; i++) {
			switch (args[i]) {
			case "-file":
				if (i + 1 < args.length) {
					excelFilePath = args[++i];
				} else {
					System.err.println(ANSI_RED + "Error: -file option requires an excel file path." + ANSI_RESET);
					return;
				}
				break;
			case "-model":
				if (i + 1 < args.length) {
					modelName = args[++i];
				} else {
					System.err.println(ANSI_RED + "Error: -model option requires a model name." + ANSI_RESET);
					return;
				}
				break;
			case "-baseurl":
				if (i + 1 < args.length && !args[i + 1].startsWith("-")) {
					baseUrl = args[++i];
				} else {
					System.err.println(ANSI_RED + "Error: -baseurl option requires a URL to follow." + ANSI_RESET);
					return;
				}
				break;
			case "-verbose":
				verbose = true; // 设置verbose为true
				break;
			default:
				System.err.println(ANSI_RED + "Error: Unknown argument " + args[i] + ANSI_RESET);
				return;
			}
		}

		// 使用verbose参数控制日志输出的详细程度
		printLog("Verbose mode is enabled.", verbose);

		// 检查文件名称及路径
		if (excelFilePath == null || excelFilePath.trim().isEmpty()) {
			System.err.println(ANSI_RED + "Error: The path to the excel file is required." + ANSI_RESET);
			return;
		}
		printLog("Excel path: " + excelFilePath, verbose);

		// 检查文件名称及路径
		if (OPENAI_API_KEY == null || OPENAI_API_KEY.trim().isEmpty()) {
			System.err.println(ANSI_RED + "Error: The OpenAI Key is required." + ANSI_RESET);
			return;
		}

		// 检查模型名称及路径
		printLog("Model name: " + modelName, verbose);

		// 检查baseUrl及路径
		if (baseUrl == null || baseUrl.trim().isEmpty()) {
			baseUrl = OPENAI_BASE_URL;
		}
		if (baseUrl == null || baseUrl.trim().isEmpty()) {
			baseUrl = OPENAI_BASE_URL_DEFAULT;
		}
		String requestURL = baseUrl + (baseUrl.endsWith("/") ? "" : "/") + OPENAI_BASE_URL_POST;
		printLog("BaseURL: " + baseUrl, verbose);
		printLog("RequestURL: " + requestURL, verbose);

		// 运行程序
		runProgram(excelFilePath, modelName, requestURL, verbose);

	}

	private static void printLog(String log, boolean verbose) {
		if (verbose) {
			System.out.println(log);
		}
	}

	private static void printUsage() {
		System.out.println("Usage: java -jar chatgptTool-<version>.jar -file <excel_file_path> [options]");
		System.out.println("Options:");
		System.out.println("  -file <excel_file_path>    Path to the excel file (required)");
		System.out.println("  -model <model_name>        Name of the model to use (optional)");
		System.out.println("                             Example:gpt-3.5-turbo");
		System.out.println("                             Example:gpt-4-1106-preview");
		System.out.println("  -baseurl <base_url>        Base URL for the service (optional).");
		System.out.println("                             First :console arg");
		System.out.println("                             Second:env[OPENAI_BASE_URL]");
		System.out.println("                             Third :default(https://api.openai.com)");
		System.out.println("  -verbose                   Enable verbose output (optional)");
	}

	private static void runProgram(String excelFilePath, String modelName, String requestURL, boolean verbose) {
		System.out.println("Start...");
		try (FileInputStream file = new FileInputStream(excelFilePath); Workbook workbook = new XSSFWorkbook(file)) {
			Sheet sheet = workbook.getSheetAt(0);
			boolean changesMade = false;

			for (Row row : sheet) {
				if (row.getRowNum() == 0)
					continue; // 跳过第一行

				Cell statusCell = row.getCell(2);
				String status = statusCell == null ? "" : statusCell.getStringCellValue();

				if (STATUS_TODO.equals(status) || status.isEmpty()) {
					try {
						// 获取序号。如果第一类为数字，则使用第一列；否则使用行号
						Cell numCell = row.getCell(0);
						double num = 0;
						if (numCell != null && numCell.getCellType() == CellType.NUMERIC)
							num = numCell.getNumericCellValue();
						int rowNum = row.getRowNum() + 1;

						// 获取问题
						Cell questionCell = row.getCell(1);
						String question = questionCell.getStringCellValue();

						if (numCell == null || num == 0)
							System.out.println(ANSI_GREEN + "rowNum:" + rowNum + " ;question:" + question + ANSI_RESET);
						else
							System.out.println(ANSI_GREEN + "num:" + (int) num + " ;question:" + question + ANSI_RESET);

						String answer = askOpenAI(modelName, requestURL, question, verbose);
						row.createCell(3).setCellValue(answer);
						row.createCell(2).setCellValue(STATUS_FINISHED);

						changesMade = true;
					} catch (Exception e) {
						row.createCell(3).setCellValue(e.getMessage());
						row.createCell(2).setCellValue(STATUS_ERROR);

						e.printStackTrace();

						// 将结果写入文件
						if (changesMade) {
							try (FileOutputStream out = new FileOutputStream(excelFilePath)) {
								workbook.write(out);
								changesMade = false;
							}
						}

						throw e;
					}
				}

				// 每隔WRITE_INTERVAL次对话写入文件；最后一次对话写入文件
				if (row.getRowNum() % WRITE_INTERVAL == 0 || row.getRowNum() == sheet.getLastRowNum()) {
					if (changesMade) {
						try (FileOutputStream out = new FileOutputStream(excelFilePath)) {
							workbook.write(out);
							changesMade = false;
						}
					}
				}

				// 增加延迟
				Thread.sleep(50);
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("finished!");
	}

	private static String buildJsonBody(String modelName, String question) {
		// 创建包含单个消息的JSONArray
		JSONArray messages = new JSONArray();
		JSONObject message = new JSONObject();
		message.put("role", "user");
		message.put("content", question);
		messages.put(message);

		// 创建最外层的JSONObject，并加入"model"和"messages"
		JSONObject requestBody = new JSONObject();
		requestBody.put("model", modelName);
		requestBody.put("messages", messages);
		String jsonBody = requestBody.toString();
		return jsonBody;
	}

	private static String askOpenAI(String modelName, String requestURL, String question, boolean verbose)
			throws IOException {
		// 定义MediaType为JSON
		MediaType MEDIA_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");
		RequestBody body = RequestBody.create(MEDIA_TYPE_JSON, buildJsonBody(modelName, question));

		Request request = new Request.Builder().url(requestURL).addHeader("Authorization", "Bearer " + OPENAI_API_KEY)
				.post(body).build();

		// 发送请求
		try (Response response = httpClient.newCall(request).execute()) {
			if (!response.isSuccessful())
				throw new IOException("Unexpected code " + response);

			JSONObject jsonObject = new JSONObject(response.body().string());
			String answer = jsonObject.getJSONArray("choices").getJSONObject(0).getJSONObject("message")
					.getString("content");
			printLog(ANSI_YELLOW + answer + ANSI_RESET, verbose);
			return answer;
		}
	}
}
