# chatgptTool

## 项目简介

调用OpenAI接口，实现基于excel记录的问题进行批量问答的工具。

特性：

- 完备的使用说明
- 可配置的参数：支持对BaseURL、Key、模型、文件名的配置
- 批量回答问题：读取excel中的待询问问题，调用OpenAI接口进行对话，将答复记录到Excel中



## 使用说明

运行环境要求：

- JDK8及以上
- 可访问OpenAI服务，或者通过反向代理访问OpenAI服务



使用说明：

```
java -jar chatgptTool-<version>.jar -file <excel_file_path> [options]
Options:
  -file <excel_file_path>    Path to the excel file (required)
  -model <model_name>        Name of the model to use (optional)
                             Example:gpt-3.5-turbo
                             Example:gpt-4-1106-preview
  -baseurl <base_url>        Base URL for the service (optional).
                             First :console arg
                             Second:env[OPENAI_BASE_URL]
                             Third :default(https://api.openai.com)
  -verbose                   Enable verbose output (optional)
```

配置说明：

- 在操作系统环境变量中配置OpenAI的key，否则执行报错。变量名称：OPENAI_API_KEY
- 在操作系统环境变量中配置OpenAI的baseURL。变量名称：OPENAI_BASE_URL
- baseURL的优先级：从高到低
  - 在命令行参数指定-baseurl <base_url>
  - 环境变量的OPENAI_BASE_URL
  - OpenAI的默认地址：https://api.openai.com
  - 注：baseURL中不要增加“v1”后缀！！
- 模型的优先级：从高到低
  - 在命令行参数指定-model <model_name>
  - 默认的模型：gpt-3.5-turbo



批量回答问题，执行逻辑：

- Excel中第一个sheet页：第一列为序号，第二列为待回答的问题，第三列为状态，第四列为回答的结果
  - 第一行的标题内容不影响执行过程，仅用于展示
  - 序号建议从1开始，整型数字。为空，或者为其他值，不影响执行过程，只用于输出过程的日志信息
  - 状态：待回答、已完成、发生错误
- 从excel第二行开始执行，执行顺序：
  - 读取第二列的待回答问题，调用OpenAI的接口进行对话，回答的结果记录到第四列，并更新第三列的状态为“已完成”
  - 如果调用过程报错，将报错信息记录到第四列，并更新第三列的状态为“发生错误”，停止执行
  - 如果第三列为“待回答”或者为空，才对这一行进行问答；否则跳过这一行

Excel示例：

| 序号 | 问题 | 状态 | 回答 |
| ---- | ---- | ---- | ---- |
|      |      |      |      |
|      |      |      |      |
|      |      |      |      |



## 未来规划

对现有程序进行完善：

- 批量回答问题
  - 对各种网络异常情况的处理
  - 加入重试机制
  - 支持System prmpt的配置
  - 支持temperature、token数量的配置
  - 支持非OpenAI的大语言模型
  - 对输出文件格式的支持：Word、MarkDown、PDF等
- 新特性：
  - 测试OpenAI各类接口的可用性
- 技术架构完善：
  - 增加必要的代码注释
  - 对依赖的第三方jar进行更新：截止至V0.9.0版本发布，直接依赖第三方jar均为最新版本（不排除间接依赖jar可能存在非最新版本情况）



## 发布历史

### V0.9.0

2024年1月24日：首次发布



## 日志输出

本章节以一个典型的批量问答形式excel为例，说明在正常、异常情况下的日志输出。用于测试的excel文件“D:\question.xlsx”内容如下：

| 序号 | 问题                                     | 状态 | 回答 |
| ---- | ---------------------------------------- | ---- | ---- |
| 1    | 软件安全领域，什么是SAST？使用中文回答。 |      |      |
| 2    | 软件安全领域，什么是DAST？               |      |      |
| 3    | 软件安全领域，什么是IAST？               |      |      |
| 4    | 软件安全领域，什么是SCA？                |      |      |



### 正常输出

正常情况下的控制台输出（基于V0.9.0版本）。



不使用-verbose：

```
java -jar chatgptTool-0.9.0.jar -file "D:\question.xlsx"
Start...
num:1 ;question:软件安全领域，什么是SAST？使用中文回答。
num:2 ;question:软件安全领域，什么是DAST？
num:3 ;question:软件安全领域，什么是IAST？
num:4 ;question:软件安全领域，什么是SCA？
finished!
```

使用-verbose：

```
java -jar chatgptTool-0.9.0.jar -file "D:\question.xlsx" -verbose
Verbose mode is enabled.
Excel path: D:\question.xlsx
Model name: gpt-3.5-turbo
BaseURL: https://gptapi.resoftcss.com.cn
RequestURL: https://gptapi.resoftcss.com.cn/v1/chat/completions
Start...
num:1 ;question:软件安全领域，什么是SAST？使用中文回答。
SAST（Static Application Security Testing）指的是静态应用程序安全测试，是一种通过分析源代码或可执行文件进行安全漏洞检测 和代码质量分析的方法。通过SAST，开发人员可以在应用程序发布之前识别和修复潜在的安全问题。

SAST可以帮助开发人员识别常见的安全漏洞，比如缓冲区溢出、注入攻击、跨站脚本（XSS）等。它可以检查代码中的漏洞、错误和弱点 ，并提供建议和修复措施来解决这些问题。

SAST工具在应用程序的源代码、二进制文件或字节码中进行扫描和分析。这些工具使用预定义的规则和模式匹配来检查代码中的安全问题。SAST工具可以自动化扫描大量的代码，并生成详细的报告，指出代码中存在的安全问题。

SAST在软件开发生命周期的早期阶段就可以使用，可以帮助开发人员及时修复潜在的安全问题，从而降低开发周期中的成本和风险。然而，SAST并不能完全替代其他安全测试方法，如动态应用程序安全测试（DAST）和手动代码审查。
num:2 ;question:软件安全领域，什么是DAST？
DAST是Dynamic Application Security Testing的缩写，意为动态应用程序安全测试。DAST是一种软件安全测试方法，通过模拟黑客攻击和探测应用程序的漏洞，评估应用程序的安全性。

DAST测试通常在应用程序运行时进行，通过向应用程序发送输入数据以触发漏洞，并分析应用程序的行为和响应来识别潜在的安全问题。DAST测试可以帮助发现常见的安全漏洞，如跨站点脚本（XSS）、SQL注入、命令注入等，并生成详细的漏洞报告。

DAST测试通过模拟真实攻击者的行为来评估应用程序的安全性，因此可以发现实际运行中的漏洞和弱点。与静态应用程序安全测试（SAST）方法相比，DAST测试更注重于应用程序的运行时行为，可以捕捉到运行时产生的漏洞和错误。

DAST测试在软件开发过程中的不同阶段都可以进行，包括开发期、测试期和部署期。通过使用DAST测试工具，组织可以提前发现和修复应用程序中的安全问题，降低应用程序被攻击的风险，并提高软件的安全性。
num:3 ;question:软件安全领域，什么是IAST？
IAST 是指交互式应用程序安全测试（Interactive Application Security Testing）的缩写。它是一种软件安全测试方法，通过在应用 程序中实时监测和分析安全漏洞，以帮助发现和修复潜在的安全问题。

相比于传统的静态应用程序安全测试（SAST）和动态应用程序安全测试（DAST），IAST 采用了一种混合的方法。在应用程序运行时，IAST 可以通过与应用程序进行交互的方式来检测各种安全漏洞，包括代码注入、跨站脚本攻击、跨站请求伪造等。它能实时检测应用程序的行为，并提供详细的报告和警告，帮助开发人员快速定位和修复安全漏洞。

IAST 的工作原理通常涉及将安全测试工具嵌入到应用程序中，这些工具可以与应用程序的运行环境进行交互，并监测和分析应用程序的 行为。常见的一些技术包括源代码注入、字节码注入、应用程序二进制接口（API）注入等。

总而言之，IAST 在软件安全领域是一种可以实时监测和分析应用程序安全漏洞的测试方法，它结合了静态和动态测试的优点，可以帮助 开发人员更快地发现和修复潜在的安全问题。
num:4 ;question:软件安全领域，什么是SCA？
在软件安全领域，SCA是指软件组件分析（Software Component Analysis）。

软件组件分析是一种用于识别软件中潜在弱点和安全漏洞的技术。它通过对软件的代码、依赖关系以及第三方组件进行分析，来检测和识别其中的安全问题。通过对软件组件进行静态或动态的分析，SCA可以观察和评估软件在运行时的行为，以发现可能的安全漏洞和攻击面 。

SCA旨在帮助软件开发人员和安全专家找到软件中潜在的安全风险，并提供建议和解决方案来优化软件的安全性。它可以揭示与第三方组 件相关的脆弱性、开放端口、敏感数据传输等问题，并帮助开发人员修复这些问题，以提高软件的安全性和可靠性。

总之，SCA是一种用于识别软件中的安全问题和漏洞的技术，它是软件安全领域中的重要方法之一。
finished!
```



### 报错信息

典型错误情况下的控制台输出（基于V0.9.0版本）。



未指定文件名：

```
java -jar chatgptTool-0.9.0.jar -file
Error: -file option requires an excel file path.
```

无效的文件名：

```
java -jar chatgptTool-0.9.0.jar -file "D:\question.xlsx"
Start...
java.io.FileNotFoundException: D:\question.xlsx (系统找不到指定的文件。)
        at java.io.FileInputStream.open0(Native Method)
        at java.io.FileInputStream.open(FileInputStream.java:195)
        at java.io.FileInputStream.<init>(FileInputStream.java:138)
        at java.io.FileInputStream.<init>(FileInputStream.java:93)
        at com.resoft.chatgpt.Chatgpt4Excel.runProgram(Chatgpt4Excel.java:151)
        at com.resoft.chatgpt.Chatgpt4Excel.main(Chatgpt4Excel.java:125)
finished!
```

未指定模型名：

```
java -jar chatgptTool-0.9.0.jar -file "D:\question.xlsx" -model
Error: -model option requires a model name.
```

无效的模型名：

```
java -jar chatgptTool-0.9.0.jar -file "D:\question.xlsx" -model xxx
Start...
num:1 ;question:介绍你自己。
java.io.IOException: Unexpected code Response{protocol=h2, code=503, message=, url=https://gptapi.resoftcss.com.cn/v1/chat/completions}
        at com.resoft.chatgpt.Chatgpt4Excel.askOpenAI(Chatgpt4Excel.java:254)
        at com.resoft.chatgpt.Chatgpt4Excel.runProgram(Chatgpt4Excel.java:180)
        at com.resoft.chatgpt.Chatgpt4Excel.main(Chatgpt4Excel.java:125)
java.io.IOException: Unexpected code Response{protocol=h2, code=503, message=, url=https://gptapi.resoftcss.com.cn/v1/chat/completions}
        at com.resoft.chatgpt.Chatgpt4Excel.askOpenAI(Chatgpt4Excel.java:254)
        at com.resoft.chatgpt.Chatgpt4Excel.runProgram(Chatgpt4Excel.java:180)
        at com.resoft.chatgpt.Chatgpt4Excel.main(Chatgpt4Excel.java:125)
```

无效的BaseURL：

```
java -jar chatgptTool-0.9.0.jar -file "D:\question.xlsx" -baseurl https://xxx
Start...
num:1 ;question:介绍你自己。
java.net.UnknownHostException: xxx
        at java.net.Inet6AddressImpl.lookupAllHostAddr(Native Method)
        at java.net.InetAddress$2.lookupAllHostAddr(InetAddress.java:929)
        at java.net.InetAddress.getAddressesFromNameService(InetAddress.java:1324)
        at java.net.InetAddress.getAllByName0(InetAddress.java:1277)
        at java.net.InetAddress.getAllByName(InetAddress.java:1193)
        at java.net.InetAddress.getAllByName(InetAddress.java:1127)
        at okhttp3.Dns$Companion$DnsSystem.lookup(Dns.kt:49)
        at okhttp3.internal.connection.RouteSelector.resetNextInetSocketAddress(RouteSelector.kt:169)
        at okhttp3.internal.connection.RouteSelector.nextProxy(RouteSelector.kt:131)
        at okhttp3.internal.connection.RouteSelector.next(RouteSelector.kt:73)
        at okhttp3.internal.connection.ExchangeFinder.findConnection(ExchangeFinder.kt:205)
        at okhttp3.internal.connection.ExchangeFinder.findHealthyConnection(ExchangeFinder.kt:106)
        at okhttp3.internal.connection.ExchangeFinder.find(ExchangeFinder.kt:74)
        at okhttp3.internal.connection.RealCall.initExchange$okhttp(RealCall.kt:255)
        at okhttp3.internal.connection.ConnectInterceptor.intercept(ConnectInterceptor.kt:32)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.cache.CacheInterceptor.intercept(CacheInterceptor.kt:95)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.http.BridgeInterceptor.intercept(BridgeInterceptor.kt:83)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.http.RetryAndFollowUpInterceptor.intercept(RetryAndFollowUpInterceptor.kt:76)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.connection.RealCall.getResponseWithInterceptorChain$okhttp(RealCall.kt:201)
        at okhttp3.internal.connection.RealCall.execute(RealCall.kt:154)
        at com.resoft.chatgpt.Chatgpt4Excel.askOpenAI(Chatgpt4Excel.java:253)
        at com.resoft.chatgpt.Chatgpt4Excel.runProgram(Chatgpt4Excel.java:180)
        at com.resoft.chatgpt.Chatgpt4Excel.main(Chatgpt4Excel.java:125)
java.net.UnknownHostException: xxx
        at java.net.Inet6AddressImpl.lookupAllHostAddr(Native Method)
        at java.net.InetAddress$2.lookupAllHostAddr(InetAddress.java:929)
        at java.net.InetAddress.getAddressesFromNameService(InetAddress.java:1324)
        at java.net.InetAddress.getAllByName0(InetAddress.java:1277)
        at java.net.InetAddress.getAllByName(InetAddress.java:1193)
        at java.net.InetAddress.getAllByName(InetAddress.java:1127)
        at okhttp3.Dns$Companion$DnsSystem.lookup(Dns.kt:49)
        at okhttp3.internal.connection.RouteSelector.resetNextInetSocketAddress(RouteSelector.kt:169)
        at okhttp3.internal.connection.RouteSelector.nextProxy(RouteSelector.kt:131)
        at okhttp3.internal.connection.RouteSelector.next(RouteSelector.kt:73)
        at okhttp3.internal.connection.ExchangeFinder.findConnection(ExchangeFinder.kt:205)
        at okhttp3.internal.connection.ExchangeFinder.findHealthyConnection(ExchangeFinder.kt:106)
        at okhttp3.internal.connection.ExchangeFinder.find(ExchangeFinder.kt:74)
        at okhttp3.internal.connection.RealCall.initExchange$okhttp(RealCall.kt:255)
        at okhttp3.internal.connection.ConnectInterceptor.intercept(ConnectInterceptor.kt:32)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.cache.CacheInterceptor.intercept(CacheInterceptor.kt:95)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.http.BridgeInterceptor.intercept(BridgeInterceptor.kt:83)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.http.RetryAndFollowUpInterceptor.intercept(RetryAndFollowUpInterceptor.kt:76)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.connection.RealCall.getResponseWithInterceptorChain$okhttp(RealCall.kt:201)
        at okhttp3.internal.connection.RealCall.execute(RealCall.kt:154)
        at com.resoft.chatgpt.Chatgpt4Excel.askOpenAI(Chatgpt4Excel.java:253)
        at com.resoft.chatgpt.Chatgpt4Excel.runProgram(Chatgpt4Excel.java:180)
        at com.resoft.chatgpt.Chatgpt4Excel.main(Chatgpt4Excel.java:125)
finished!
```

指定错误格式的BaseURL（https://gptapi.resoftcss.com.cn/v1）：

```
java -jar chatgptTool-0.9.0.jar -file "D:\question.xlsx.xlsx"
Start...
num:1 ;question:介绍你自己。
java.io.IOException: Unexpected code Response{protocol=h2, code=404, message=, url=https://gptapi.resoftcss.com.cn/v1/v1/chat/completions}
        at com.resoft.chatgpt.Chatgpt4Excel.askOpenAI(Chatgpt4Excel.java:254)
        at com.resoft.chatgpt.Chatgpt4Excel.runProgram(Chatgpt4Excel.java:180)
        at com.resoft.chatgpt.Chatgpt4Excel.main(Chatgpt4Excel.java:125)
java.io.IOException: Unexpected code Response{protocol=h2, code=404, message=, url=https://gptapi.resoftcss.com.cn/v1/v1/chat/completions}
        at com.resoft.chatgpt.Chatgpt4Excel.askOpenAI(Chatgpt4Excel.java:254)
        at com.resoft.chatgpt.Chatgpt4Excel.runProgram(Chatgpt4Excel.java:180)
        at com.resoft.chatgpt.Chatgpt4Excel.main(Chatgpt4Excel.java:125)
finished!
```

未明确指定BaseURL，默认BaseURL无法访问（https://api.openai.com）：

```
java -jar chatgptTool-0.9.0.jar -file "D:\question.xlsx"
Start...
num:1 ;question:介绍你自己。
java.net.ConnectException: Failed to connect to api.openai.com/205.186.152.122:443
        at okhttp3.internal.connection.RealConnection.connectSocket(RealConnection.kt:297)
        at okhttp3.internal.connection.RealConnection.connect(RealConnection.kt:207)
        at okhttp3.internal.connection.ExchangeFinder.findConnection(ExchangeFinder.kt:226)
        at okhttp3.internal.connection.ExchangeFinder.findHealthyConnection(ExchangeFinder.kt:106)
        at okhttp3.internal.connection.ExchangeFinder.find(ExchangeFinder.kt:74)
        at okhttp3.internal.connection.RealCall.initExchange$okhttp(RealCall.kt:255)
        at okhttp3.internal.connection.ConnectInterceptor.intercept(ConnectInterceptor.kt:32)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.cache.CacheInterceptor.intercept(CacheInterceptor.kt:95)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.http.BridgeInterceptor.intercept(BridgeInterceptor.kt:83)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.http.RetryAndFollowUpInterceptor.intercept(RetryAndFollowUpInterceptor.kt:76)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.connection.RealCall.getResponseWithInterceptorChain$okhttp(RealCall.kt:201)
        at okhttp3.internal.connection.RealCall.execute(RealCall.kt:154)
        at com.resoft.chatgpt.Chatgpt4Excel.askOpenAI(Chatgpt4Excel.java:253)
        at com.resoft.chatgpt.Chatgpt4Excel.runProgram(Chatgpt4Excel.java:180)
        at com.resoft.chatgpt.Chatgpt4Excel.main(Chatgpt4Excel.java:125)
Caused by: java.net.ConnectException: Connection timed out: connect
        at java.net.DualStackPlainSocketImpl.waitForConnect(Native Method)
        at java.net.DualStackPlainSocketImpl.socketConnect(DualStackPlainSocketImpl.java:81)
        at java.net.AbstractPlainSocketImpl.doConnect(AbstractPlainSocketImpl.java:476)
        at java.net.AbstractPlainSocketImpl.connectToAddress(AbstractPlainSocketImpl.java:218)
        at java.net.AbstractPlainSocketImpl.connect(AbstractPlainSocketImpl.java:200)
        at java.net.PlainSocketImpl.connect(PlainSocketImpl.java:162)
        at java.net.SocksSocketImpl.connect(SocksSocketImpl.java:394)
        at java.net.Socket.connect(Socket.java:606)
        at okhttp3.internal.platform.Platform.connectSocket(Platform.kt:128)
        at okhttp3.internal.connection.RealConnection.connectSocket(RealConnection.kt:295)
        ... 18 more
java.net.ConnectException: Failed to connect to api.openai.com/205.186.152.122:443
        at okhttp3.internal.connection.RealConnection.connectSocket(RealConnection.kt:297)
        at okhttp3.internal.connection.RealConnection.connect(RealConnection.kt:207)
        at okhttp3.internal.connection.ExchangeFinder.findConnection(ExchangeFinder.kt:226)
        at okhttp3.internal.connection.ExchangeFinder.findHealthyConnection(ExchangeFinder.kt:106)
        at okhttp3.internal.connection.ExchangeFinder.find(ExchangeFinder.kt:74)
        at okhttp3.internal.connection.RealCall.initExchange$okhttp(RealCall.kt:255)
        at okhttp3.internal.connection.ConnectInterceptor.intercept(ConnectInterceptor.kt:32)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.cache.CacheInterceptor.intercept(CacheInterceptor.kt:95)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.http.BridgeInterceptor.intercept(BridgeInterceptor.kt:83)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.http.RetryAndFollowUpInterceptor.intercept(RetryAndFollowUpInterceptor.kt:76)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.connection.RealCall.getResponseWithInterceptorChain$okhttp(RealCall.kt:201)
        at okhttp3.internal.connection.RealCall.execute(RealCall.kt:154)
        at com.resoft.chatgpt.Chatgpt4Excel.askOpenAI(Chatgpt4Excel.java:253)
        at com.resoft.chatgpt.Chatgpt4Excel.runProgram(Chatgpt4Excel.java:180)
        at com.resoft.chatgpt.Chatgpt4Excel.main(Chatgpt4Excel.java:125)
Caused by: java.net.ConnectException: Connection timed out: connect
        at java.net.DualStackPlainSocketImpl.waitForConnect(Native Method)
        at java.net.DualStackPlainSocketImpl.socketConnect(DualStackPlainSocketImpl.java:81)
        at java.net.AbstractPlainSocketImpl.doConnect(AbstractPlainSocketImpl.java:476)
        at java.net.AbstractPlainSocketImpl.connectToAddress(AbstractPlainSocketImpl.java:218)
        at java.net.AbstractPlainSocketImpl.connect(AbstractPlainSocketImpl.java:200)
        at java.net.PlainSocketImpl.connect(PlainSocketImpl.java:162)
        at java.net.SocksSocketImpl.connect(SocksSocketImpl.java:394)
        at java.net.Socket.connect(Socket.java:606)
        at okhttp3.internal.platform.Platform.connectSocket(Platform.kt:128)
        at okhttp3.internal.connection.RealConnection.connectSocket(RealConnection.kt:295)
        ... 18 more
finished!
```

无效的BaseURL：

```
java -jar chatgptTool-0.9.0.jar -file "D:\question.xlsx" -baseurl https://xxx
Start...
num:1 ;question:介绍你自己。
java.net.UnknownHostException: xxx
        at java.net.Inet6AddressImpl.lookupAllHostAddr(Native Method)
        at java.net.InetAddress$2.lookupAllHostAddr(InetAddress.java:929)
        at java.net.InetAddress.getAddressesFromNameService(InetAddress.java:1324)
        at java.net.InetAddress.getAllByName0(InetAddress.java:1277)
        at java.net.InetAddress.getAllByName(InetAddress.java:1193)
        at java.net.InetAddress.getAllByName(InetAddress.java:1127)
        at okhttp3.Dns$Companion$DnsSystem.lookup(Dns.kt:49)
        at okhttp3.internal.connection.RouteSelector.resetNextInetSocketAddress(RouteSelector.kt:169)
        at okhttp3.internal.connection.RouteSelector.nextProxy(RouteSelector.kt:131)
        at okhttp3.internal.connection.RouteSelector.next(RouteSelector.kt:73)
        at okhttp3.internal.connection.ExchangeFinder.findConnection(ExchangeFinder.kt:205)
        at okhttp3.internal.connection.ExchangeFinder.findHealthyConnection(ExchangeFinder.kt:106)
        at okhttp3.internal.connection.ExchangeFinder.find(ExchangeFinder.kt:74)
        at okhttp3.internal.connection.RealCall.initExchange$okhttp(RealCall.kt:255)
        at okhttp3.internal.connection.ConnectInterceptor.intercept(ConnectInterceptor.kt:32)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.cache.CacheInterceptor.intercept(CacheInterceptor.kt:95)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.http.BridgeInterceptor.intercept(BridgeInterceptor.kt:83)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.http.RetryAndFollowUpInterceptor.intercept(RetryAndFollowUpInterceptor.kt:76)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.connection.RealCall.getResponseWithInterceptorChain$okhttp(RealCall.kt:201)
        at okhttp3.internal.connection.RealCall.execute(RealCall.kt:154)
        at com.resoft.chatgpt.Chatgpt4Excel.askOpenAI(Chatgpt4Excel.java:253)
        at com.resoft.chatgpt.Chatgpt4Excel.runProgram(Chatgpt4Excel.java:180)
        at com.resoft.chatgpt.Chatgpt4Excel.main(Chatgpt4Excel.java:125)
java.net.UnknownHostException: xxx
        at java.net.Inet6AddressImpl.lookupAllHostAddr(Native Method)
        at java.net.InetAddress$2.lookupAllHostAddr(InetAddress.java:929)
        at java.net.InetAddress.getAddressesFromNameService(InetAddress.java:1324)
        at java.net.InetAddress.getAllByName0(InetAddress.java:1277)
        at java.net.InetAddress.getAllByName(InetAddress.java:1193)
        at java.net.InetAddress.getAllByName(InetAddress.java:1127)
        at okhttp3.Dns$Companion$DnsSystem.lookup(Dns.kt:49)
        at okhttp3.internal.connection.RouteSelector.resetNextInetSocketAddress(RouteSelector.kt:169)
        at okhttp3.internal.connection.RouteSelector.nextProxy(RouteSelector.kt:131)
        at okhttp3.internal.connection.RouteSelector.next(RouteSelector.kt:73)
        at okhttp3.internal.connection.ExchangeFinder.findConnection(ExchangeFinder.kt:205)
        at okhttp3.internal.connection.ExchangeFinder.findHealthyConnection(ExchangeFinder.kt:106)
        at okhttp3.internal.connection.ExchangeFinder.find(ExchangeFinder.kt:74)
        at okhttp3.internal.connection.RealCall.initExchange$okhttp(RealCall.kt:255)
        at okhttp3.internal.connection.ConnectInterceptor.intercept(ConnectInterceptor.kt:32)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.cache.CacheInterceptor.intercept(CacheInterceptor.kt:95)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.http.BridgeInterceptor.intercept(BridgeInterceptor.kt:83)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.http.RetryAndFollowUpInterceptor.intercept(RetryAndFollowUpInterceptor.kt:76)
        at okhttp3.internal.http.RealInterceptorChain.proceed(RealInterceptorChain.kt:109)
        at okhttp3.internal.connection.RealCall.getResponseWithInterceptorChain$okhttp(RealCall.kt:201)
        at okhttp3.internal.connection.RealCall.execute(RealCall.kt:154)
        at com.resoft.chatgpt.Chatgpt4Excel.askOpenAI(Chatgpt4Excel.java:253)
        at com.resoft.chatgpt.Chatgpt4Excel.runProgram(Chatgpt4Excel.java:180)
        at com.resoft.chatgpt.Chatgpt4Excel.main(Chatgpt4Excel.java:125)
finished!
```

未指定OpenAI Key：

```
java -jar chatgptTool-0.9.0.jar -file "D:\question.xlsx"
Error: The OpenAI Key is required.
```

无效的OpenAI Key（通过OneAPI反向代理）：

```
java -jar chatgptTool-0.9.0.jar -file "D:\question.xlsx"
Start...
num:1 ;question:介绍你自己。
java.io.IOException: Unexpected code Response{protocol=h2, code=401, message=, url=https://gptapi.resoftcss.com.cn/v1/chat/completions}
        at com.resoft.chatgpt.Chatgpt4Excel.askOpenAI(Chatgpt4Excel.java:254)
        at com.resoft.chatgpt.Chatgpt4Excel.runProgram(Chatgpt4Excel.java:180)
        at com.resoft.chatgpt.Chatgpt4Excel.main(Chatgpt4Excel.java:125)
java.io.IOException: Unexpected code Response{protocol=h2, code=401, message=, url=https://gptapi.resoftcss.com.cn/v1/chat/completions}
        at com.resoft.chatgpt.Chatgpt4Excel.askOpenAI(Chatgpt4Excel.java:254)
        at com.resoft.chatgpt.Chatgpt4Excel.runProgram(Chatgpt4Excel.java:180)
        at com.resoft.chatgpt.Chatgpt4Excel.main(Chatgpt4Excel.java:125)
finished!
```

